defmodule Tipid.Repo do
  use Ecto.Repo,
    otp_app: :tipid,
    adapter: Ecto.Adapters.Postgres
end
