defmodule Tipid.AccountsTest do
  use Tipid.DataCase

  alias Tipid.Accounts
  alias Tipid.Accounts.User

  describe "users" do
    test "create_user/1 with valid data creates a user" do
      attrs = %{
        email: "testuser@example.com",
        password: "password",
        password_confirmation: "password"
      }

      {:ok, %User{} = user} = Accounts.create_user(attrs)

      assert user.email == "testuser@example.com"
      assert user.password == "password"
      refute user.password_hash == "password"
      refute user.password_hash == ""
    end

    test "create_user/1 with duplicate email returns error changeset" do
      Factory.insert!(:user, %{email: "testuser@example.com"})

      attrs = %{
        email: "testuser@example.com",
        password: "password",
        password_confirmation: "password"
      }

      {:error, %Ecto.Changeset{} = changeset} = Accounts.create_user(attrs)

      assert Enum.count(changeset.errors) == 1

      assert changeset.errors[:email] ==
               {"has already been taken",
                [constraint: :unique, constraint_name: "users_email_index"]}
    end

    test "create_user/1 with very short password returns error changeset" do
      attrs = %{
        email: "testuser@example.com",
        password: "passwd",
        password_confirmation: "passwd"
      }

      {:error, %Ecto.Changeset{} = changeset} = Accounts.create_user(attrs)

      assert Enum.count(changeset.errors) == 1

      assert changeset.errors[:password] ==
               {"should be at least %{count} character(s)",
                [count: 8, validation: :length, kind: :min, type: :string]}
    end

    test "create_user/1 with very long password returns error changeset" do
      attrs = %{
        email: "testuser@example.com",
        password: "thisisthelongestpasswordeverthatwascreatedbysomepersonhereonearth",
        password_confirmation: "thisisthelongestpasswordeverthatwascreatedbysomepersonhereonearth"
      }

      {:error, %Ecto.Changeset{} = changeset} = Accounts.create_user(attrs)

      assert Enum.count(changeset.errors) == 1

      assert changeset.errors[:password] ==
               {"should be at most %{count} character(s)",
                [count: 30, validation: :length, kind: :max, type: :string]}
    end

    test "create_user/1 with non-matching password returns error changeset" do
      attrs = %{
        email: "testuser@example.com",
        password: "password123",
        password_confirmation: "password456"
      }

      {:error, %Ecto.Changeset{} = changeset} = Accounts.create_user(attrs)

      assert Enum.count(changeset.errors) == 1

      assert changeset.errors[:password_confirmation] ==
               {"does not match confirmation", [validation: :confirmation]}
    end

    test "create_user/1 with blank data returns error changeset" do
      attrs = %{
        email: "",
        password: "",
        password_confirmation: ""
      }

      {:error, %Ecto.Changeset{} = changeset} = Accounts.create_user(attrs)

      assert Enum.count(changeset.errors) == 2
      assert changeset.errors[:email] == {"can't be blank", [validation: :required]}
      assert changeset.errors[:password] == {"can't be blank", [validation: :required]}
    end
  end
end
