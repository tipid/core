defmodule Tipid.Factory do
  alias Tipid.Accounts.User
  alias Tipid.Repo

  def build(factory_name, attrs) do
    factory_name
    |> build()
    |> struct(attrs)
  end

  def insert!(factory_name, attrs \\ []) do
    factory_name
    |> build(attrs)
    |> Repo.insert!()
  end

  def build(:user) do
    %User{email: "test#{System.unique_integer([:positive])}@example.com"}
  end
end
